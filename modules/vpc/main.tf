resource "aws_vpc" "myvpc" {
  cidr_block = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support = true

  tags = {
    Name = "${var.project_name}-vpc"
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id =  aws_vpc.myvpc.id

  tags = {
    Name = "${var.project_name}-igw"
  }
}

data "aws_availability_zones" "available_zones" {}

resource "aws_subnet" "pub_sub_1a" {
  vpc_id = aws_vpc.myvpc.id
  cidr_block = var.pub_sub_1a_cidr
  availability_zone = data.aws_availability_zones.available_zones.names[0]
  map_public_ip_on_launch = true

  tags = {
    Name = "pub_sub_1a"
  }
}

resource "aws_subnet" "pub_sub_2b" {
  vpc_id = aws_vpc.myvpc.id
  cidr_block = var.pub_sub_2b_cidr
  availability_zone = data.aws_availability_zones.available_zones.names[1]
  map_public_ip_on_launch = true

  tags = {
    Name = "pub_sub_2b"
  }
}

resource "aws_route_table" "rt" {
  vpc_id  =aws_vpc.myvpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = "public-route-table"
  }
}

# associate public subnet pub_sub_1a to route_table

resource "aws_route_table_association" "pub_sub_1a_rta" {
  subnet_id = aws_subnet.pub_sub_1a.id
  route_table_id = aws_route_table.rt.id
}
# associate public subnet pub_sub_2b to route_table

resource "aws_route_table_association" "pub_sub_2b_rta" {
  subnet_id = aws_subnet.pub_sub_2b.id
  route_table_id = aws_route_table.rt.id
}

# create private app subnet pri_sub_3a
resource "aws_subnet" "pri_sub_3a" {
  vpc_id = aws_vpc.myvpc.id
  cidr_block = var.pri_sub_3a_cidr
  availability_zone = data.aws_availability_zones.available_zones.names[0]
  map_public_ip_on_launch = false
  
  tags = {
    Name = "pri-sub-3a"
  }
}

# create private app subnet pri_sub_4b
resource "aws_subnet" "pri_sub_4b" {
  vpc_id = aws_vpc.myvpc.id
  cidr_block = var.pri_sub_4b_cidr
  availability_zone = data.aws_availability_zones.available_zones.names[1]
  map_public_ip_on_launch = false
  
  tags = {
    Name = "pri-sub-4b"
  }
}

# create private DB subnet pri_sub_5a
resource "aws_subnet" "pri_sub_5a" {
  vpc_id = aws_vpc.myvpc.id
  cidr_block = var.pri_sub_5a_cidr
  availability_zone = data.aws_availability_zones.available_zones.names[0]
  map_public_ip_on_launch = false
  
  tags = {
    Name = "pri-sub-5b"
  }
}

# create private DB subnet pri_sub_6b
resource "aws_subnet" "pri_sub_6b" {
  vpc_id = aws_vpc.myvpc.id
  cidr_block = var.pri_sub_6b_cidr
  availability_zone = data.aws_availability_zones.available_zones.names[1]
  map_public_ip_on_launch = false
  
  tags = {
    Name = "pri-sub-6b"
  }
}

/*resource "aws_subnet" "pub-sub" {
  vpc_id = aws_vpc.myvpc.id
  cidr_block = "10.0.1.0/24"
  map_public_ip_on_launch = true
  availability_zone = "ap-southeast-2a"

  tags = {
    Name = "pub_sub1"
  }
}

resource "aws_security_group" "sg" {
    vpc_id = aws_vpc.myvpc.id
    name = "mysg"

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
  
}*/