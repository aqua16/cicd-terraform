terraform {
  backend "s3" {
    bucket = "statelockterraform"
    key = "state"
    region = "ap-southeast-2"
    dynamodb_table = "terraform-state-lock"
  }
}